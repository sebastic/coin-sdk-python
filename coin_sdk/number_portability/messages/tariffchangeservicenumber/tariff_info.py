# coding: utf-8

"""
    COIN CRDB Rest API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0.0
    Contact: servicedesk@coin.nl
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class TariffInfo(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'peak': 'str',
        'offpeak': 'str',
        'currency': 'str',
        'type': 'str',
        'vat': 'str'
    }

    attribute_map = {
        'peak': 'peak',
        'offpeak': 'offpeak',
        'currency': 'currency',
        'type': 'type',
        'vat': 'vat'
    }

    def __init__(self, peak=None, offpeak=None, currency=None, type=None, vat=None):
        """
        TariffInfo - a model defined in Swagger
        """

        self._peak = None
        self._offpeak = None
        self._currency = None
        self._type = None
        self._vat = None

        self.peak = peak
        self.offpeak = offpeak
        self.currency = currency
        self.type = type
        self.vat = vat

    @property
    def peak(self):
        """
        Gets the peak of this TariffInfo.

        :return: The peak of this TariffInfo.
        :rtype: str
        """
        return self._peak

    @peak.setter
    def peak(self, peak):
        """
        Sets the peak of this TariffInfo.

        :param peak: The peak of this TariffInfo.
        :type: str
        """
        if peak is None:
            raise ValueError("Invalid value for `peak`, must not be `None`")
        if peak is not None and not re.search('^[,0-9]{7,8}$', peak):
            raise ValueError("Invalid value for `peak`, must be a follow pattern or equal to `/^[,0-9]{7,8}$/`")

        self._peak = peak

    @property
    def offpeak(self):
        """
        Gets the offpeak of this TariffInfo.

        :return: The offpeak of this TariffInfo.
        :rtype: str
        """
        return self._offpeak

    @offpeak.setter
    def offpeak(self, offpeak):
        """
        Sets the offpeak of this TariffInfo.

        :param offpeak: The offpeak of this TariffInfo.
        :type: str
        """
        if offpeak is None:
            raise ValueError("Invalid value for `offpeak`, must not be `None`")
        if offpeak is not None and not re.search('^[,0-9]{7,8}$', offpeak):
            raise ValueError("Invalid value for `offpeak`, must be a follow pattern or equal to `/^[,0-9]{7,8}$/`")

        self._offpeak = offpeak

    @property
    def currency(self):
        """
        Gets the currency of this TariffInfo.

        :return: The currency of this TariffInfo.
        :rtype: str
        """
        return self._currency

    @currency.setter
    def currency(self, currency):
        """
        Sets the currency of this TariffInfo.

        :param currency: The currency of this TariffInfo.
        :type: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")
        allowed_values = ["0", "1"]
        if currency not in allowed_values:
            raise ValueError(
                "Invalid value for `currency` ({0}), must be one of {1}"
                .format(currency, allowed_values)
            )

        self._currency = currency

    @property
    def type(self):
        """
        Gets the type of this TariffInfo.

        :return: The type of this TariffInfo.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """
        Sets the type of this TariffInfo.

        :param type: The type of this TariffInfo.
        :type: str
        """
        if type is None:
            raise ValueError("Invalid value for `type`, must not be `None`")

        self._type = type

    @property
    def vat(self):
        """
        Gets the vat of this TariffInfo.

        :return: The vat of this TariffInfo.
        :rtype: str
        """
        return self._vat

    @vat.setter
    def vat(self, vat):
        """
        Sets the vat of this TariffInfo.

        :param vat: The vat of this TariffInfo.
        :type: str
        """
        if vat is None:
            raise ValueError("Invalid value for `vat`, must not be `None`")

        self._vat = vat

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, TariffInfo):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
