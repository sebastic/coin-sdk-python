# coding: utf-8

"""
    COIN CRDB Rest API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0.0
    Contact: servicedesk@coin.nl
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class EnumProfileActivation(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'dossierid': 'str',
        'currentnetworkoperator': 'str',
        'typeofnumber': 'str',
        'scope': 'str',
        'profileid': 'str',
        'ttl': 'str',
        'dnsclass': 'str',
        'rectype': 'str',
        'order': 'str',
        'preference': 'str',
        'flags': 'str',
        'enumservice': 'str',
        'regexp': 'str',
        'usertag': 'str',
        'domain': 'str',
        'spcode': 'str',
        'processtype': 'str',
        'gateway': 'str',
        'service': 'str',
        'domaintag': 'str',
        'replacement': 'str'
    }

    attribute_map = {
        'dossierid': 'dossierid',
        'currentnetworkoperator': 'currentnetworkoperator',
        'typeofnumber': 'typeofnumber',
        'scope': 'scope',
        'profileid': 'profileid',
        'ttl': 'ttl',
        'dnsclass': 'dnsclass',
        'rectype': 'rectype',
        'order': 'order',
        'preference': 'preference',
        'flags': 'flags',
        'enumservice': 'enumservice',
        'regexp': 'regexp',
        'usertag': 'usertag',
        'domain': 'domain',
        'spcode': 'spcode',
        'processtype': 'processtype',
        'gateway': 'gateway',
        'service': 'service',
        'domaintag': 'domaintag',
        'replacement': 'replacement'
    }

    def __init__(self, dossierid=None, currentnetworkoperator=None, typeofnumber=None, scope=None, profileid=None, ttl=None, dnsclass=None, rectype=None, order=None, preference=None, flags=None, enumservice=None, regexp=None, usertag=None, domain=None, spcode=None, processtype=None, gateway=None, service=None, domaintag=None, replacement=None):
        """
        EnumProfileActivation - a model defined in Swagger
        """

        self._dossierid = None
        self._currentnetworkoperator = None
        self._typeofnumber = None
        self._scope = None
        self._profileid = None
        self._ttl = None
        self._dnsclass = None
        self._rectype = None
        self._order = None
        self._preference = None
        self._flags = None
        self._enumservice = None
        self._regexp = None
        self._usertag = None
        self._domain = None
        self._spcode = None
        self._processtype = None
        self._gateway = None
        self._service = None
        self._domaintag = None
        self._replacement = None

        self.dossierid = dossierid
        self.currentnetworkoperator = currentnetworkoperator
        self.typeofnumber = typeofnumber
        self.scope = scope
        self.profileid = profileid
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rectype = rectype
        if order is not None:
          self.order = order
        if preference is not None:
          self.preference = preference
        if flags is not None:
          self.flags = flags
        if enumservice is not None:
          self.enumservice = enumservice
        if regexp is not None:
          self.regexp = regexp
        if usertag is not None:
          self.usertag = usertag
        if domain is not None:
          self.domain = domain
        if spcode is not None:
          self.spcode = spcode
        if processtype is not None:
          self.processtype = processtype
        if gateway is not None:
          self.gateway = gateway
        if service is not None:
          self.service = service
        if domaintag is not None:
          self.domaintag = domaintag
        if replacement is not None:
          self.replacement = replacement

    @property
    def dossierid(self):
        """
        Gets the dossierid of this EnumProfileActivation.

        :return: The dossierid of this EnumProfileActivation.
        :rtype: str
        """
        return self._dossierid

    @dossierid.setter
    def dossierid(self, dossierid):
        """
        Sets the dossierid of this EnumProfileActivation.

        :param dossierid: The dossierid of this EnumProfileActivation.
        :type: str
        """
        if dossierid is None:
            raise ValueError("Invalid value for `dossierid`, must not be `None`")
        if dossierid is not None and not re.search('^[-0-9A-Z]{5,26}$', dossierid):
            raise ValueError("Invalid value for `dossierid`, must be a follow pattern or equal to `/^[-0-9A-Z]{5,26}$/`")

        self._dossierid = dossierid

    @property
    def currentnetworkoperator(self):
        """
        Gets the currentnetworkoperator of this EnumProfileActivation.

        :return: The currentnetworkoperator of this EnumProfileActivation.
        :rtype: str
        """
        return self._currentnetworkoperator

    @currentnetworkoperator.setter
    def currentnetworkoperator(self, currentnetworkoperator):
        """
        Sets the currentnetworkoperator of this EnumProfileActivation.

        :param currentnetworkoperator: The currentnetworkoperator of this EnumProfileActivation.
        :type: str
        """
        if currentnetworkoperator is None:
            raise ValueError("Invalid value for `currentnetworkoperator`, must not be `None`")
        if currentnetworkoperator is not None and not re.search('^[0-9A-Z]{3,6}$', currentnetworkoperator):
            raise ValueError("Invalid value for `currentnetworkoperator`, must be a follow pattern or equal to `/^[0-9A-Z]{3,6}$/`")

        self._currentnetworkoperator = currentnetworkoperator

    @property
    def typeofnumber(self):
        """
        Gets the typeofnumber of this EnumProfileActivation.

        :return: The typeofnumber of this EnumProfileActivation.
        :rtype: str
        """
        return self._typeofnumber

    @typeofnumber.setter
    def typeofnumber(self, typeofnumber):
        """
        Sets the typeofnumber of this EnumProfileActivation.

        :param typeofnumber: The typeofnumber of this EnumProfileActivation.
        :type: str
        """
        if typeofnumber is None:
            raise ValueError("Invalid value for `typeofnumber`, must not be `None`")

        self._typeofnumber = typeofnumber

    @property
    def scope(self):
        """
        Gets the scope of this EnumProfileActivation.

        :return: The scope of this EnumProfileActivation.
        :rtype: str
        """
        return self._scope

    @scope.setter
    def scope(self, scope):
        """
        Sets the scope of this EnumProfileActivation.

        :param scope: The scope of this EnumProfileActivation.
        :type: str
        """
        if scope is None:
            raise ValueError("Invalid value for `scope`, must not be `None`")
        if scope is not None and not re.search('^[-0-9A-Z]{1,40}$', scope):
            raise ValueError("Invalid value for `scope`, must be a follow pattern or equal to `/^[-0-9A-Z]{1,40}$/`")

        self._scope = scope

    @property
    def profileid(self):
        """
        Gets the profileid of this EnumProfileActivation.

        :return: The profileid of this EnumProfileActivation.
        :rtype: str
        """
        return self._profileid

    @profileid.setter
    def profileid(self, profileid):
        """
        Sets the profileid of this EnumProfileActivation.

        :param profileid: The profileid of this EnumProfileActivation.
        :type: str
        """
        if profileid is None:
            raise ValueError("Invalid value for `profileid`, must not be `None`")
        if profileid is not None and not re.search('^[-0-9A-Z]{5,26}$', profileid):
            raise ValueError("Invalid value for `profileid`, must be a follow pattern or equal to `/^[-0-9A-Z]{5,26}$/`")

        self._profileid = profileid

    @property
    def ttl(self):
        """
        Gets the ttl of this EnumProfileActivation.

        :return: The ttl of this EnumProfileActivation.
        :rtype: str
        """
        return self._ttl

    @ttl.setter
    def ttl(self, ttl):
        """
        Sets the ttl of this EnumProfileActivation.

        :param ttl: The ttl of this EnumProfileActivation.
        :type: str
        """
        if ttl is None:
            raise ValueError("Invalid value for `ttl`, must not be `None`")

        self._ttl = ttl

    @property
    def dnsclass(self):
        """
        Gets the dnsclass of this EnumProfileActivation.

        :return: The dnsclass of this EnumProfileActivation.
        :rtype: str
        """
        return self._dnsclass

    @dnsclass.setter
    def dnsclass(self, dnsclass):
        """
        Sets the dnsclass of this EnumProfileActivation.

        :param dnsclass: The dnsclass of this EnumProfileActivation.
        :type: str
        """
        if dnsclass is None:
            raise ValueError("Invalid value for `dnsclass`, must not be `None`")
        if dnsclass is not None and len(dnsclass) > 40:
            raise ValueError("Invalid value for `dnsclass`, length must be less than or equal to `40`")
        if dnsclass is not None and len(dnsclass) < 1:
            raise ValueError("Invalid value for `dnsclass`, length must be greater than or equal to `1`")

        self._dnsclass = dnsclass

    @property
    def rectype(self):
        """
        Gets the rectype of this EnumProfileActivation.

        :return: The rectype of this EnumProfileActivation.
        :rtype: str
        """
        return self._rectype

    @rectype.setter
    def rectype(self, rectype):
        """
        Sets the rectype of this EnumProfileActivation.

        :param rectype: The rectype of this EnumProfileActivation.
        :type: str
        """
        if rectype is None:
            raise ValueError("Invalid value for `rectype`, must not be `None`")
        if rectype is not None and len(rectype) > 40:
            raise ValueError("Invalid value for `rectype`, length must be less than or equal to `40`")
        if rectype is not None and len(rectype) < 1:
            raise ValueError("Invalid value for `rectype`, length must be greater than or equal to `1`")

        self._rectype = rectype

    @property
    def order(self):
        """
        Gets the order of this EnumProfileActivation.

        :return: The order of this EnumProfileActivation.
        :rtype: str
        """
        return self._order

    @order.setter
    def order(self, order):
        """
        Sets the order of this EnumProfileActivation.

        :param order: The order of this EnumProfileActivation.
        :type: str
        """

        self._order = order

    @property
    def preference(self):
        """
        Gets the preference of this EnumProfileActivation.

        :return: The preference of this EnumProfileActivation.
        :rtype: str
        """
        return self._preference

    @preference.setter
    def preference(self, preference):
        """
        Sets the preference of this EnumProfileActivation.

        :param preference: The preference of this EnumProfileActivation.
        :type: str
        """

        self._preference = preference

    @property
    def flags(self):
        """
        Gets the flags of this EnumProfileActivation.

        :return: The flags of this EnumProfileActivation.
        :rtype: str
        """
        return self._flags

    @flags.setter
    def flags(self, flags):
        """
        Sets the flags of this EnumProfileActivation.

        :param flags: The flags of this EnumProfileActivation.
        :type: str
        """
        if flags is not None and len(flags) > 40:
            raise ValueError("Invalid value for `flags`, length must be less than or equal to `40`")
        if flags is not None and len(flags) < 1:
            raise ValueError("Invalid value for `flags`, length must be greater than or equal to `1`")

        self._flags = flags

    @property
    def enumservice(self):
        """
        Gets the enumservice of this EnumProfileActivation.

        :return: The enumservice of this EnumProfileActivation.
        :rtype: str
        """
        return self._enumservice

    @enumservice.setter
    def enumservice(self, enumservice):
        """
        Sets the enumservice of this EnumProfileActivation.

        :param enumservice: The enumservice of this EnumProfileActivation.
        :type: str
        """
        if enumservice is not None and len(enumservice) > 65:
            raise ValueError("Invalid value for `enumservice`, length must be less than or equal to `65`")
        if enumservice is not None and len(enumservice) < 1:
            raise ValueError("Invalid value for `enumservice`, length must be greater than or equal to `1`")

        self._enumservice = enumservice

    @property
    def regexp(self):
        """
        Gets the regexp of this EnumProfileActivation.

        :return: The regexp of this EnumProfileActivation.
        :rtype: str
        """
        return self._regexp

    @regexp.setter
    def regexp(self, regexp):
        """
        Sets the regexp of this EnumProfileActivation.

        :param regexp: The regexp of this EnumProfileActivation.
        :type: str
        """
        if regexp is not None and len(regexp) > 400:
            raise ValueError("Invalid value for `regexp`, length must be less than or equal to `400`")
        if regexp is not None and len(regexp) < 1:
            raise ValueError("Invalid value for `regexp`, length must be greater than or equal to `1`")

        self._regexp = regexp

    @property
    def usertag(self):
        """
        Gets the usertag of this EnumProfileActivation.

        :return: The usertag of this EnumProfileActivation.
        :rtype: str
        """
        return self._usertag

    @usertag.setter
    def usertag(self, usertag):
        """
        Sets the usertag of this EnumProfileActivation.

        :param usertag: The usertag of this EnumProfileActivation.
        :type: str
        """
        if usertag is not None and len(usertag) > 400:
            raise ValueError("Invalid value for `usertag`, length must be less than or equal to `400`")
        if usertag is not None and len(usertag) < 1:
            raise ValueError("Invalid value for `usertag`, length must be greater than or equal to `1`")

        self._usertag = usertag

    @property
    def domain(self):
        """
        Gets the domain of this EnumProfileActivation.

        :return: The domain of this EnumProfileActivation.
        :rtype: str
        """
        return self._domain

    @domain.setter
    def domain(self, domain):
        """
        Sets the domain of this EnumProfileActivation.

        :param domain: The domain of this EnumProfileActivation.
        :type: str
        """
        if domain is not None and len(domain) > 255:
            raise ValueError("Invalid value for `domain`, length must be less than or equal to `255`")
        if domain is not None and len(domain) < 1:
            raise ValueError("Invalid value for `domain`, length must be greater than or equal to `1`")

        self._domain = domain

    @property
    def spcode(self):
        """
        Gets the spcode of this EnumProfileActivation.

        :return: The spcode of this EnumProfileActivation.
        :rtype: str
        """
        return self._spcode

    @spcode.setter
    def spcode(self, spcode):
        """
        Sets the spcode of this EnumProfileActivation.

        :param spcode: The spcode of this EnumProfileActivation.
        :type: str
        """
        if spcode is not None and len(spcode) > 40:
            raise ValueError("Invalid value for `spcode`, length must be less than or equal to `40`")
        if spcode is not None and len(spcode) < 1:
            raise ValueError("Invalid value for `spcode`, length must be greater than or equal to `1`")

        self._spcode = spcode

    @property
    def processtype(self):
        """
        Gets the processtype of this EnumProfileActivation.

        :return: The processtype of this EnumProfileActivation.
        :rtype: str
        """
        return self._processtype

    @processtype.setter
    def processtype(self, processtype):
        """
        Sets the processtype of this EnumProfileActivation.

        :param processtype: The processtype of this EnumProfileActivation.
        :type: str
        """
        if processtype is not None and len(processtype) > 40:
            raise ValueError("Invalid value for `processtype`, length must be less than or equal to `40`")
        if processtype is not None and len(processtype) < 1:
            raise ValueError("Invalid value for `processtype`, length must be greater than or equal to `1`")

        self._processtype = processtype

    @property
    def gateway(self):
        """
        Gets the gateway of this EnumProfileActivation.

        :return: The gateway of this EnumProfileActivation.
        :rtype: str
        """
        return self._gateway

    @gateway.setter
    def gateway(self, gateway):
        """
        Sets the gateway of this EnumProfileActivation.

        :param gateway: The gateway of this EnumProfileActivation.
        :type: str
        """
        if gateway is not None and len(gateway) > 40:
            raise ValueError("Invalid value for `gateway`, length must be less than or equal to `40`")
        if gateway is not None and len(gateway) < 1:
            raise ValueError("Invalid value for `gateway`, length must be greater than or equal to `1`")

        self._gateway = gateway

    @property
    def service(self):
        """
        Gets the service of this EnumProfileActivation.

        :return: The service of this EnumProfileActivation.
        :rtype: str
        """
        return self._service

    @service.setter
    def service(self, service):
        """
        Sets the service of this EnumProfileActivation.

        :param service: The service of this EnumProfileActivation.
        :type: str
        """
        if service is not None and len(service) > 400:
            raise ValueError("Invalid value for `service`, length must be less than or equal to `400`")
        if service is not None and len(service) < 1:
            raise ValueError("Invalid value for `service`, length must be greater than or equal to `1`")

        self._service = service

    @property
    def domaintag(self):
        """
        Gets the domaintag of this EnumProfileActivation.

        :return: The domaintag of this EnumProfileActivation.
        :rtype: str
        """
        return self._domaintag

    @domaintag.setter
    def domaintag(self, domaintag):
        """
        Sets the domaintag of this EnumProfileActivation.

        :param domaintag: The domaintag of this EnumProfileActivation.
        :type: str
        """
        if domaintag is not None and len(domaintag) > 400:
            raise ValueError("Invalid value for `domaintag`, length must be less than or equal to `400`")
        if domaintag is not None and len(domaintag) < 1:
            raise ValueError("Invalid value for `domaintag`, length must be greater than or equal to `1`")

        self._domaintag = domaintag

    @property
    def replacement(self):
        """
        Gets the replacement of this EnumProfileActivation.

        :return: The replacement of this EnumProfileActivation.
        :rtype: str
        """
        return self._replacement

    @replacement.setter
    def replacement(self, replacement):
        """
        Sets the replacement of this EnumProfileActivation.

        :param replacement: The replacement of this EnumProfileActivation.
        :type: str
        """
        if replacement is not None and len(replacement) > 400:
            raise ValueError("Invalid value for `replacement`, length must be less than or equal to `400`")
        if replacement is not None and len(replacement) < 1:
            raise ValueError("Invalid value for `replacement`, length must be greater than or equal to `1`")

        self._replacement = replacement

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, EnumProfileActivation):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
