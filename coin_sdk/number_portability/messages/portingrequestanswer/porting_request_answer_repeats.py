# coding: utf-8

"""
    Coin CRDB Rest API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 3.0.0
    Contact: devops@coin.nl
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint

import six

from coin_sdk.number_portability.messages.portingrequestanswer.porting_request_answer_seq import PortingRequestAnswerSeq  # noqa: F401,E501


class PortingRequestAnswerRepeats(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'seq': 'PortingRequestAnswerSeq'
    }

    attribute_map = {
        'seq': 'seq'
    }

    def __init__(self, seq=None):  # noqa: E501
        """PortingRequestAnswerRepeats - a model defined in Swagger"""  # noqa: E501

        self._seq = None
        self.discriminator = None

        self.seq = seq

    @property
    def seq(self):
        """Gets the seq of this PortingRequestAnswerRepeats.  # noqa: E501


        :return: The seq of this PortingRequestAnswerRepeats.  # noqa: E501
        :rtype: PortingRequestAnswerSeq
        """
        return self._seq

    @seq.setter
    def seq(self, seq):
        """Sets the seq of this PortingRequestAnswerRepeats.


        :param seq: The seq of this PortingRequestAnswerRepeats.  # noqa: E501
        :type: PortingRequestAnswerSeq
        """
        if seq is None:
            raise ValueError("Invalid value for `seq`, must not be `None`")  # noqa: E501

        self._seq = seq

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PortingRequestAnswerRepeats, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PortingRequestAnswerRepeats):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
